--
-- PostgreSQL database dump
--

-- Dumped from database version 15.0 (Debian 15.0-1.pgdg110+1)
-- Dumped by pg_dump version 15.0 (Debian 15.0-1.pgdg110+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: cours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cours (
                              id integer NOT NULL,
                              cours_code character varying(200) NOT NULL,
                              cours_name character varying(200) NOT NULL
);


ALTER TABLE public.cours OWNER TO postgres;

--
-- Name: cours_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.cours_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cours_id_seq OWNER TO postgres;

--
-- Name: cours_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.cours_id_seq OWNED BY public.cours.id;


--
-- Name: cours_to_etudiant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.cours_to_etudiant (
                                          cours_id integer,
                                          etudiant_id integer
);


ALTER TABLE public.cours_to_etudiant OWNER TO postgres;

--
-- Name: enseignant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.enseignant (
                                   id integer NOT NULL,
                                   lastname character varying(200) NOT NULL,
                                   firstname character varying(200) NOT NULL
);


ALTER TABLE public.enseignant OWNER TO postgres;

--
-- Name: enseignant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.enseignant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.enseignant_id_seq OWNER TO postgres;

--
-- Name: enseignant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.enseignant_id_seq OWNED BY public.enseignant.id;


--
-- Name: enseignant_to_cours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.enseignant_to_cours (
                                            enseignant_id integer NOT NULL,
                                            cours_id integer NOT NULL
);


ALTER TABLE public.enseignant_to_cours OWNER TO postgres;

--
-- Name: etudiant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.etudiant (
                                 id integer NOT NULL,
                                 lastname character varying(200),
                                 firstname character varying(200)
);


ALTER TABLE public.etudiant OWNER TO postgres;

--
-- Name: etudiant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.etudiant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.etudiant_id_seq OWNER TO postgres;

--
-- Name: etudiant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.etudiant_id_seq OWNED BY public.etudiant.id;


--
-- Name: formation; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.formation (
                                  id integer NOT NULL,
                                  name character varying(200) NOT NULL
);


ALTER TABLE public.formation OWNER TO postgres;

--
-- Name: formation_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.formation_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formation_id_seq OWNER TO postgres;

--
-- Name: formation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.formation_id_seq OWNED BY public.formation.id;


--
-- Name: formation_to_cours; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.formation_to_cours (
                                           formation_id integer NOT NULL,
                                           cours_id integer NOT NULL
);


ALTER TABLE public.formation_to_cours OWNER TO postgres;

--
-- Name: formation_to_enseignant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.formation_to_enseignant (
                                                formation_id integer,
                                                enseignant_id integer
);


ALTER TABLE public.formation_to_enseignant OWNER TO postgres;

--
-- Name: cours id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cours ALTER COLUMN id SET DEFAULT nextval('public.cours_id_seq'::regclass);


--
-- Name: enseignant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enseignant ALTER COLUMN id SET DEFAULT nextval('public.enseignant_id_seq'::regclass);


--
-- Name: etudiant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.etudiant ALTER COLUMN id SET DEFAULT nextval('public.etudiant_id_seq'::regclass);


--
-- Name: formation id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formation ALTER COLUMN id SET DEFAULT nextval('public.formation_id_seq'::regclass);


--
-- Data for Name: cours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cours (id, cours_code, cours_name) FROM stdin;
1       C43     Cours d'OS
2       A78     Physique classique
3       M89     Couture moléculaire
4       M12     Chapellerie
\.


--
-- Data for Name: cours_to_etudiant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.cours_to_etudiant (cours_id, etudiant_id) FROM stdin;
1       3
1       4
3       6
4       6
4       7
\.


--
-- Data for Name: enseignant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.enseignant (id, lastname, firstname) FROM stdin;
3       Smith   John
4       Smithy  Johnathan
7       Bo      Jum
8       Mouse   Mickey
6       Picsou  Balthazar
\.


--
-- Data for Name: enseignant_to_cours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.enseignant_to_cours (enseignant_id, cours_id) FROM stdin;
3       1
7       4
\.


--
-- Data for Name: etudiant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.etudiant (id, lastname, firstname) FROM stdin;
3       Doe     Maria
4       Doe     Mary
5       Smith   Mary
6       Design  Sophie
7       Duck    Donald
\.


--
-- Data for Name: formation; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.formation (id, name) FROM stdin;
2       Mode
1       Ingénieur
\.


--
-- Data for Name: formation_to_cours; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.formation_to_cours (formation_id, cours_id) FROM stdin;
1       1
2       3
2       4
\.


--
-- Data for Name: formation_to_enseignant; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.formation_to_enseignant (formation_id, enseignant_id) FROM stdin;
1       3
2       7
2       8
\.


--
-- Name: cours_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.cours_id_seq', 7, true);


--
-- Name: enseignant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.enseignant_id_seq', 11, true);


--
-- Name: etudiant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.etudiant_id_seq', 9, true);


--
-- Name: formation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.formation_id_seq', 10, true);


--
-- Name: cours cours_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cours
    ADD CONSTRAINT cours_pk PRIMARY KEY (id);


--
-- Name: enseignant enseignant_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enseignant
    ADD CONSTRAINT enseignant_pk PRIMARY KEY (id);


--
-- Name: etudiant etudiants_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.etudiant
    ADD CONSTRAINT etudiants_pk PRIMARY KEY (id);


--
-- Name: formation formation_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formation
    ADD CONSTRAINT formation_pk PRIMARY KEY (id);


--
-- Name: cours_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX cours_id_uindex ON public.cours USING btree (id);


--
-- Name: enseignant_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX enseignant_id_uindex ON public.enseignant USING btree (id);


--
-- Name: formation_id_uindex; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX formation_id_uindex ON public.formation USING btree (id);


--
-- Name: formation_to_cours cours_id___fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formation_to_cours
    ADD CONSTRAINT cours_id___fk FOREIGN KEY (cours_id) REFERENCES public.cours(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: enseignant_to_cours cours_id__fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enseignant_to_cours
    ADD CONSTRAINT cours_id__fk FOREIGN KEY (cours_id) REFERENCES public.cours(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cours_to_etudiant cours_to_etudiant_cours_null_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cours_to_etudiant
    ADD CONSTRAINT cours_to_etudiant_cours_null_fk FOREIGN KEY (cours_id) REFERENCES public.cours(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: cours_to_etudiant cours_to_etudiant_etudiant_null_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.cours_to_etudiant
    ADD CONSTRAINT cours_to_etudiant_etudiant_null_fk FOREIGN KEY (etudiant_id) REFERENCES public.etudiant(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: formation_to_enseignant enseignant_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formation_to_enseignant
    ADD CONSTRAINT enseignant_id_fk FOREIGN KEY (enseignant_id) REFERENCES public.enseignant(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: enseignant_to_cours enseignant_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.enseignant_to_cours
    ADD CONSTRAINT enseignant_id_fk FOREIGN KEY (enseignant_id) REFERENCES public.enseignant(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: formation_to_cours formation_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formation_to_cours
    ADD CONSTRAINT formation_id_fk FOREIGN KEY (formation_id) REFERENCES public.formation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: formation_to_enseignant formation_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.formation_to_enseignant
    ADD CONSTRAINT formation_id_fk FOREIGN KEY (formation_id) REFERENCES public.formation(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- PostgreSQL database dump complete
--