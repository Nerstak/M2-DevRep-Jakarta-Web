# M2 DevRep Jakarta Web: Gestion de cours et formation (TP)

## Description

Ce TP permet la gestion de cours, formation, étudiants et enseignants avec Jakarta 9 et une BDD PostgresSQL.
Il permet la suppression, l'ajout et la recherche de données, avec les JPA et des EntityManagers



## Installation

### Base de données

L'utilisateur PostgreSQL par défaut est `postgres` avec pour mot de passe `docker`. La commande suivante permet de créer de créer une instance avec les bons paramètres :

```
docker run --name pg-docker -e POSTGRES_PASSWORD=docker -d -p 5432:5432 -v $HOME/docker/volumes/postgres:/var/lib/postgresql/data postgres
```

Il faut ensuite créer une base `sorbonne_university`.



Le fichier `db.sql` provient d'un pg_dump de la base de données (schémas et données). Il est possible d'importer ce fichier avec :

```
psql -U postgres sorbonne_university < db.sql
```

### Déploiement

Pour créer le war exploded :

```
maven package
```

Le war est généré dans `target/jakartaTp-1.0-SNAPSHOT`. Déployer ce dossier dans un Web Container (Tomcat par exemple).
