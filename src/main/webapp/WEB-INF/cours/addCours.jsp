<html>
<head>
    <title>Gérer cours</title>
    <%@include file="../includes/bootstrapCSS.jsp" %>
</head>
<body>
<%@include file="../includes/navBar.jsp" %>
<%@include file="../includes/infoBox.jsp" %>

<%@ include file="includes/addCours.jsp" %>
<%@ include file="includes/addEtudiantCours.jsp" %>

<%@include file="../includes/bootstrapJS.jsp" %>
</body>
</html>
