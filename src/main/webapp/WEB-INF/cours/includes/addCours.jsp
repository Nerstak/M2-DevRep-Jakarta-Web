<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <h1 class="display-5 p-1">Créer cours</h1>
    <form action="" method="post" class="container-fluid">
        <div class="row mb-3">
            <label for="codeCours" class="col-sm-2 col-form-label">Code du cours</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="codeCours" id="codeCours"
                       placeholder="C43"/>
            </div>
        </div>
        <div class="row mb-3">
            <label for="nameCours" class="col-sm-2 col-form-label">Nom du cours</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="nameCours" id="nameCours"
                       placeholder="Physique fondamentale"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-2">
                <button type="submit" class="btn btn-primary" name="actionForm" value="add">Créer</button>
            </div>
        </div>
    </form>
</div>