<c:if test="${param.typeSelected == null || param.typeSelected == '' ||param.typeSelected == 'cours'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Code du cours</th>
            <th>Nom du cours</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="co" items="${requestScope.coursToStudents}">
            <tr>
                <td>${co.getCode()}</td>
                <td>${co.getName()}</td>
                <td>
                    <form action="cours" method="post">
                        <input type="hidden" name="coursId" value="${co.getId()}">
                        <button type="submit" class="btn btn-danger" name="actionForm" value="delete">Supprimer</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>