<form action="cours" method="get" class="container-fluid">
    <div class="p-2 my-2 bg-light rounded-3">
        <div class="row align-items-center g-3">
            <div class="col-auto">
                <label class="visually-hidden" for="codeCours">Code du Cours</label>
                <input type="text" class="form-control" id="codeCours" name="codeCours"
                       placeholder="C43" value="${param.codeCours}">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="nomCours">Nom</label>
                <input type="text" class="form-control" id="nomCours" name="nomCours"
                       placeholder="Physique fondamentale" value="${param.nomCours}">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="typeSelected">Type de visualisation</label>
                <select class="form-select" id="typeSelected" name="typeSelected" aria-label="Default select type">
                    <option
                            <c:if test="${param.typeSelected == null || param.typeSelected == '' || param.typeSelected == 'cours'}">selected</c:if>
                            value="cours">Cours
                    </option>
                    <option
                            <c:if test="${param.typeSelected == 'etudiant'}">selected</c:if>
                            value="etudiant">Assignation des étudiants
                    </option>
                </select>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary" name="action" value="show">Search</button>
            </div>
        </div>
    </div>
</form>