<c:if test="${param.typeSelected == 'etudiant'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Code du cours</th>
            <th>Nom du cours</th>
            <th>Prénom de l'étudiant</th>
            <th>Nom de l'étudiant</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="co" items="${requestScope.coursToStudents}">
            <c:forEach var="st" items="${co.getEtudiants()}">
                <tr>
                    <td>${co.getCode()}</td>
                    <td>${co.getName()}</td>
                    <td>${st.getFirstName()}</td>
                    <td>${st.getLastName()}</td>
                    <td>
                        <form action="cours" method="post">
                            <input type="hidden" name="etudiantId" value="${st.getId()}">
                            <input type="hidden" name="coursId" value="${co.getId()}">
                            <button type="submit" class="btn btn-danger" name="actionForm" value="delete-etudiant">Enlever étudiant du cours</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </c:forEach>
        </tbody>
    </table>
</c:if>