<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <h1 class="display-5 p-1">Ajouter étudiant à un cours</h1>
    <form action="" method="post" class="container-fluid">
        <div class="row mb-3">
            <label for="codeCoursE" class="col-sm-2 col-form-label">Code du cours</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="codeCours" id="codeCoursE"
                       placeholder="C43"/>
            </div>
        </div>
        <div class="row mb-3">
            <label for="firstnameStudent" class="col-sm-2 col-form-label">Prenom de l'étudiant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="firstnameEtudiant" id="firstnameStudent"
                       placeholder="John"/>
            </div>
        </div>
        <div class="row mb-3">
            <label for="lastnameStudent" class="col-sm-2 col-form-label">Nom de l'étudiant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="lastnameEtudiant" id="lastnameStudent"
                       placeholder="Doe"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-2">
                <button type="submit" class="btn btn-primary" name="actionForm" value="add-etudiant">Ajouter étudiant au cours</button>
            </div>
        </div>
    </form>
</div>