<html>
<head>
    <title>Afficher cours</title>
    <%@include file="../includes/bootstrapCSS.jsp" %>

</head>
<body>
<%@include file="../includes/navBar.jsp" %>

<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <h1 class="display-5 p-1">Liste des cours</h1>
    <%@ include file="includes/searchForm.jsp" %>

    <%@ include file="includes/showResultsSearchCours.jsp" %>
    <%@ include file="includes/showResultsSearchEtudiant.jsp" %>
</div>

<%@include file="../includes/bootstrapJS.jsp" %>
</body>
</html>
