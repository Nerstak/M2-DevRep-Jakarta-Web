<form action="formation" method="get" class="container-fluid">
    <div class="p-2 my-2 bg-light rounded-3">
        <div class="row align-items-center g-3">
            <div class="col-auto">
                <label class="visually-hidden" for="nameFormation">Prénom</label>
                <input type="text" class="form-control" id="nameFormation" name="nameFormation"
                       placeholder="Ingénieur" value="${param.nameFormation}">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="typeSelected">Type de visualisation</label>
                <select class="form-select" id="typeSelected" name="typeSelected" aria-label="Default select type">
                    <option
                            <c:if test="${param.typeSelected == null || param.typeSelected == '' || param.typeSelected == 'formation'}">selected</c:if>
                            value="formation">Formation
                    </option>
                    <option
                            <c:if test="${param.typeSelected == 'cours'}">selected</c:if>
                            value="cours">Cours
                    </option>
                    <option
                            <c:if test="${param.typeSelected == 'enseignant'}">selected</c:if>
                            value="enseignant">Enseignants
                    </option>
                    <option
                            <c:if test="${param.typeSelected == 'etudiant'}">selected</c:if>
                            value="etudiant">Étudiants
                    </option>
                </select>
            </div>

            <div class="col-auto">
                <button type="submit" class="btn btn-primary" name="action" value="show">Search</button>
            </div>
        </div>
    </div>
</form>