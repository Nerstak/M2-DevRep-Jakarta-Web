<c:if test="${param.typeSelected == 'etudiant'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Formation</th>
            <th>Prénom étudiant</th>
            <th>Nom étudiant</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="fet" items="${requestScope.formations}">
            <tr>
                <td>${fet.formation().getName()}</td>
                <td>${fet.etudiant().getFirstName()}</td>
                <td>${fet.etudiant().getLastName()}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>

<c:if test="${param.typeSelected == 'enseignant'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Formation</th>
            <th>Prénom enseignant</th>
            <th>Nom enseignant</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="f" items="${requestScope.formations}">
            <c:forEach var="st" items="${f.getEnseignants()}">
                <tr>
                    <td>${f.getName()}</td>
                    <td>${st.getFirstName()}</td>
                    <td>${st.getLastName()}</td>
                    <td>
                        <form action="formation" method="post">
                            <input type="hidden" name="formationId" value="${f.getId()}">
                            <input type="hidden" name="enseignantId" value="${st.getId()}">
                            <button type="submit" class="btn btn-danger" name="actionForm" value="delete-enseignant">Enlever enseignant de la formation</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </c:forEach>
        </tbody>
    </table>
</c:if>

<c:if test="${param.typeSelected == 'cours'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Formation</th>
            <th>Code de cours</th>
            <th>Nom de cours</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="f" items="${requestScope.formations}">
            <c:forEach var="co" items="${f.getCours()}">
                <tr>
                    <td>${f.getName()}</td>
                    <td>${co.getCode()}</td>
                    <td>${co.getName()}</td>
                    <td>
                        <form action="formation" method="post">
                            <input type="hidden" name="formationId" value="${f.getId()}">
                            <input type="hidden" name="coursId" value="${co.getId()}">
                            <button type="submit" class="btn btn-danger" name="actionForm" value="delete-cours">Enlever cours de la formation</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </c:forEach>
        </tbody>
    </table>
</c:if>

<c:if test="${param.typeSelected == null || param.typeSelected == '' || param.typeSelected == 'formation'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Formation</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="f" items="${requestScope.formations}">
            <tr>
                <td>${f.getName()}</td>
                <td>
                    <form action="formation" method="post">
                        <input type="hidden" name="formationId" value="${f.getId()}">
                        <button type="submit" class="btn btn-danger" name="actionForm" value="delete">Supprimer</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>