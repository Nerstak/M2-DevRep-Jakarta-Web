<html>
<head>
    <title>Ajouter formation</title>
    <%@include file="../includes/bootstrapCSS.jsp" %>
</head>
<body>
<%@include file="../includes/navBar.jsp" %>
<%@include file="../includes/infoBox.jsp" %>

<%@include file="./includes/addFormation.jsp" %>
<%@include file="./includes/addCoursFormation.jsp" %>
<%@include file="./includes/addEnseignantFormation.jsp" %>

<%@include file="../includes/bootstrapJS.jsp" %>
</body>
</html>
