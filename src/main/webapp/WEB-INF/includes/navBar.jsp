<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a href="./" class="navbar-brand">ClassHandler</a>
        <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <div class="navbar-nav">
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Formations</a>
                    <div class="dropdown-menu">
                        <a href="formation?action=show" class="dropdown-item">Afficher</a>
                        <a href="formation?action=add" class="dropdown-item">Ajouter</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Cours</a>
                    <div class="dropdown-menu">
                        <a href="cours?action=show" class="dropdown-item">Afficher</a>
                        <a href="cours?action=add" class="dropdown-item">Ajouter</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Enseignants</a>
                    <div class="dropdown-menu">
                        <a href="enseignant?action=show" class="dropdown-item">Afficher</a>
                        <a href="enseignant?action=add" class="dropdown-item">Ajouter</a>
                    </div>
                </div>
                <div class="nav-item dropdown">
                    <a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Étudiants</a>
                    <div class="dropdown-menu">
                        <a href="etudiant?action=show" class="dropdown-item">Afficher</a>
                        <a href="etudiant?action=add" class="dropdown-item">Ajouter</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>