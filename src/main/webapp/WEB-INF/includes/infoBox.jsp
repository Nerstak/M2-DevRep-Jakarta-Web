<c:if test="${not empty result}">
  <div class="container-sm mt-5">
    <c:choose>
      <c:when test="${status == 'success'}">
        <div class="alert alert-success alert-dismissible fade show">
          <strong>Success!</strong>
          <p><span>${result}</span></p>
        </div>
      </c:when>
      <c:when test="${status == 'error'}">
        <div class="alert alert-danger alert-dismissible fade show">
          <strong>Error!</strong>
          <p><span>${result}</span></p>
        </div>
      </c:when>
      <c:when test="${status == 'info'}">
        <div class="alert alert-info alert-dismissible fade show">
          <strong>Info!</strong>
          <p><span>${result}</span></p>
        </div>
      </c:when>
    </c:choose>
  </div>
</c:if>
