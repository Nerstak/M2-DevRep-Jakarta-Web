<!DOCTYPE html>
<html>
<head>
    <title>Page d'accueil</title>
    <%@include file="includes/bootstrapCSS.jsp" %>
</head>
<body>
<%@include file="includes/navBar.jsp" %>
<%@include file="includes/infoBox.jsp" %>
<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <p class="p-1">Site de gestion et de visualisation des cours et formations</p>
</div>

<%@include file="includes/bootstrapJS.jsp" %>
</body>
</html>