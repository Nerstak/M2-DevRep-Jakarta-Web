<html>
<head>
    <title>Gérer étudiant</title>
    <%@include file="../includes/bootstrapCSS.jsp" %>
</head>
<body>
<%@include file="../includes/navBar.jsp" %>
<%@include file="../includes/infoBox.jsp" %>
<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <h1 class="display-5 p-1">Créer étudiant</h1>

    <form action="" method="post" class="container-fluid">
        <div class="row mb-3">
            <label for="firstnameStudent" class="col-sm-2 col-form-label">Prenom de l'étudiant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="firstnameStudent" id="firstnameStudent"
                       placeholder="John"/>
            </div>
        </div>
        <div class="row mb-3">
            <label for="lastnameStudent" class="col-sm-2 col-form-label">Nom de l'étudiant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="lastnameStudent" id="lastnameStudent"
                       placeholder="Doe"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-2">
                <button type="submit" class="btn btn-primary" name="actionForm" value="add">Créer</button>
            </div>
        </div>
    </form>
</div>

<%@include file="../includes/bootstrapJS.jsp" %>
</body>
</html>
