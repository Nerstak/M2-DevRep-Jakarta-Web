<table class="table table-striped p-5 my-4 bg-light rounded-3">
    <thead>
    <tr>
        <th>Prénom</th>
        <th>Nom</th>
        <th class="w-25 p-3"></th>
    </tr>
    </thead>
    <tbody>
    <c:forEach var="et" items="${requestScope.listEtudiant}">
        <tr>
            <td>${et.getFirstName()}</td>
            <td>${et.getLastName()}</td>
            <td>
                <form action="etudiant" method="post">
                    <input type="hidden" name="etudiantId" value="${et.getId()}">
                    <button type="submit" class="btn btn-danger" name="actionForm" value="delete">Supprimer</button>
                </form>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>