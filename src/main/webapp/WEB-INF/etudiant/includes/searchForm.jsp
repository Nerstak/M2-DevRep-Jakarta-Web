<form action="etudiant" method="get" class="container-fluid">
    <div class="p-2 my-2 bg-light rounded-3">
        <div class="row align-items-center g-3">
            <input type="hidden">
            <div class="col-auto">
                <label class="visually-hidden" for="firstnameStudent">Prénom</label>
                <input type="text" class="form-control" id="firstnameStudent" name="firstnameStudent"
                       placeholder="John" value="${param.firstnameStudent}">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="lastnameStudent">Nom</label>
                <input type="text" class="form-control" id="lastnameStudent" name="lastnameStudent"
                       placeholder="Doe" value="${param.lastnameStudent}">
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary" name="action" value="show">Search</button>
            </div>
        </div>
    </div>
</form>