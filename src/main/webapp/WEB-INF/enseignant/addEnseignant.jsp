<html>
<head>
    <title>Gérer enseignants</title>
    <%@include file="../includes/bootstrapCSS.jsp" %>
</head>
<body>
<%@include file="../includes/navBar.jsp" %>
<%@include file="../includes/infoBox.jsp" %>

<%@ include file="includes/addEnseignant.jsp" %>
<%@ include file="includes/addCoursEnseignant.jsp" %>

<%@include file="../includes/bootstrapJS.jsp" %>
</body>
</html>
