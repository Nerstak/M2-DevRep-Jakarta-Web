<form action="enseignant" method="get" class="container-fluid">
    <div class="p-2 my-2 bg-light rounded-3">
        <div class="row align-items-center g-3">
            <div class="col-auto">
                <label class="visually-hidden" for="firstnameTeacher">Prénom</label>
                <input type="text" class="form-control" id="firstnameTeacher" name="firstnameTeacher"
                       placeholder="John" value="${param.firstnameTeacher}">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="lastnameTeacher">Nom</label>
                <input type="text" class="form-control" id="lastnameTeacher" name="lastnameTeacher"
                       placeholder="Doe" value="${param.lastnameTeacher}">
            </div>
            <div class="col-auto">
                <label class="visually-hidden" for="typeSelected">Type de visualisation</label>
                <select class="form-select" id="typeSelected" name="typeSelected" aria-label="Default select type">
                    <option
                            <c:if test="${param.typeSelected == null || param.typeSelected == '' || param.typeSelected == 'enseignant'}">selected</c:if>
                            value="enseignant">Enseignants
                    </option>
                    <option
                            <c:if test="${param.typeSelected == 'cours'}">selected</c:if>
                            value="cours">Assignation des cours
                    </option>
                </select>
            </div>
            <div class="col-auto">
                <button type="submit" class="btn btn-primary" name="action" value="show">Search</button>
            </div>
        </div>
    </div>
</form>