<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <h1 class="display-5 p-1">Ajouter cours à enseignant</h1>
    <form action="" method="post" class="container-fluid">
        <div class="row mb-3">
            <label for="firstnameTeacherC" class="col-sm-2 col-form-label">Prenom de l'enseignant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="firstnameTeacher" id="firstnameTeacherC"
                       placeholder="John"/>
            </div>
        </div>
        <div class="row mb-3">
            <label for="lastnameTeacherC" class="col-sm-2 col-form-label">Nom de l'enseignant</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="lastnameTeacher" id="lastnameTeacherC" placeholder="Doe"/>
            </div>
        </div>
        <div class="row mb-3">
            <label for="codeCoursC" class="col-sm-2 col-form-label">Code du cours</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="codeCours" id="codeCoursC" placeholder="C43"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 offset-sm-2">
                <button type="submit" class="btn btn-primary" name="actionForm" value="add-cours">Créer</button>
            </div>
        </div>
    </form>
</div>