<c:if test="${param.typeSelected == 'cours'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Prénom</th>
            <th>Nom</th>
            <th>Code du cours</th>
            <th>Nom du cours</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="en" items="${requestScope.enseignantToCours}">
            <c:forEach var="crs" items="${en.getCours()}">
                <tr>
                    <td>${en.getFirstName()}</td>
                    <td>${en.getLastName()}</td>
                    <td>${crs.getCode()}</td>
                    <td>${crs.getName()}</td>
                    <td>
                        <form action="enseignant" method="post">
                            <input type="hidden" name="enseignantId" value="${en.getId()}">
                            <input type="hidden" name="coursId" value="${crs.getId()}">
                            <button type="submit" class="btn btn-danger" name="actionForm" value="delete-cours">Enlever cours de l'enseignant</button>
                        </form>
                    </td>
                </tr>
            </c:forEach>
        </c:forEach>
        </tbody>
    </table>
</c:if>