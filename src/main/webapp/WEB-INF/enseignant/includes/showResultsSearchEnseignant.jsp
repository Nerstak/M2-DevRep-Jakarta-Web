<c:if test="${param.typeSelected == null || param.typeSelected == '' || param.typeSelected == 'enseignant'}">
    <table class="table table-striped p-5 my-4 bg-light rounded-3">
        <thead>
        <tr>
            <th>Prénom</th>
            <th>Nom</th>
            <th class="w-25 p-3"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="en" items="${requestScope.enseignantToCours}">
            <tr>
                <td>${en.getFirstName()}</td>
                <td>${en.getLastName()}</td>
                <td>
                    <form action="enseignant" method="post">
                        <input type="hidden" name="enseignantId" value="${en.getId()}">
                        <button type="submit" class="btn btn-danger" name="actionForm" value="delete">Supprimer</button>
                    </form>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if>