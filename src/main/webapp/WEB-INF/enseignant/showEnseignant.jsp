<html>
<head>
    <title>Afficher enseignants</title>
    <%@include file="../includes/bootstrapCSS.jsp" %>
</head>
<body>
<%@include file="../includes/navBar.jsp" %>
<div class="container-xl shadow-sm p-2 mt-5 bg-white rounded">
    <h1 class="display-5 p-1">Liste des enseignants</h1>
    <%@ include file="includes/searchForm.jsp" %>


    <%@ include file="includes/showResultsSearchEnseignant.jsp" %>
    <%@ include file="includes/showResultsSearchCoursEnseignant.jsp" %>
</div>

<%@include file="../includes/bootstrapJS.jsp" %>
</body>
</html>
