package fr.su.jakartaTp.models;

public record SimpleResult(String message, String resultType) {
}
