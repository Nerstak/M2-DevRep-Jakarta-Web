package fr.su.jakartaTp.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Table(name = "enseignant", schema = "public", catalog = "sorbonne_university")
@NamedQueries({
        @NamedQuery(name = "Enseignant.SelectAll", query = "SELECT e FROM Enseignant e"),
        @NamedQuery(name = "Enseignant.SelectByNames", query = "SELECT e FROM Enseignant e WHERE e.firstName = :fn AND e.lastName = :ln"),
        @NamedQuery(name = "Enseignant.SelectLikeByNames", query = "SELECT e FROM Enseignant e WHERE e.firstName LIKE CONCAT('%',:fn ,'%') AND e.lastName LIKE CONCAT('%',:ln ,'%')")
})
public class Enseignant implements InterfaceEntity  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Basic
    @Column(name = "lastname", nullable = false)
    String lastName;

    @Basic
    @Column(name = "firstname", nullable = false)
    String firstName;

    @ManyToMany
    @JoinTable(
            name = "enseignant_to_cours",
            joinColumns = @JoinColumn(name = "enseignant_id"),
            inverseJoinColumns = @JoinColumn(name = "cours_id")
    )
    private List<Cours> cours = new ArrayList<>();

    public Enseignant(String firstnameTeacher, String lastnameTeacher) {
        this.lastName = lastnameTeacher;
        this.firstName = firstnameTeacher;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Enseignant that = (Enseignant) obj;
        return Objects.equals(id, that.id);
    }
}
