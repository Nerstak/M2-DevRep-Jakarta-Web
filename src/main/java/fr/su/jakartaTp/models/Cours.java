package fr.su.jakartaTp.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Table(name = "cours", schema = "public", catalog = "sorbonne_university")
@NamedQueries({
        @NamedQuery(name = "Cours.SelectAll", query = "SELECT c FROM Cours c"),
        @NamedQuery(name = "Cours.SelectByCode", query = "SELECT c FROM Cours c WHERE c.code = :code"),
        @NamedQuery(name = "Cours.SelectLikeByCodeAndName", query = "SELECT c FROM Cours c WHERE c.code LIKE CONCAT('%',:code,'%') AND c.name LIKE CONCAT('%',:name,'%')")
})
public class Cours implements InterfaceEntity  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Basic
    @Column(name = "cours_code", nullable = false)
    String code;

    @Basic
    @Column(name = "cours_name", nullable = false)
    String name;

    @ManyToMany
    @JoinTable(
            name = "formation_to_cours",
            joinColumns = @JoinColumn(name = "formation_id"),
            inverseJoinColumns = @JoinColumn(name = "cours_id")
    )
    private List<Cours> cours = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "cours_to_etudiant",
            joinColumns = @JoinColumn(name = "cours_id"),
            inverseJoinColumns = @JoinColumn(name = "etudiant_id")
    )
    private List<Etudiant> etudiants = new ArrayList<>();

    public Cours(String codeCours, String nomCours) {
        this.name = nomCours;
        this.code = codeCours;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Cours that = (Cours) obj;
        return Objects.equals(id, that.id);
    }
}
