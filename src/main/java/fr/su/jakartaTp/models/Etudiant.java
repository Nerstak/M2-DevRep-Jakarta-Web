package fr.su.jakartaTp.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Objects;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Entity
@Table(name = "etudiant", schema = "public", catalog = "sorbonne_university")
@NamedQueries({
        @NamedQuery(name = "Etudiant.SelectAll", query = "SELECT e FROM Etudiant e"),
        @NamedQuery(name = "Etudiant.SelectByNames", query = "SELECT e FROM Etudiant e WHERE e.firstName = :fn AND e.lastName = :ln"),
        @NamedQuery(name = "Etudiant.SelectLikeByNames", query = "SELECT e FROM Etudiant e WHERE e.firstName LIKE CONCAT('%',:fn ,'%') AND e.lastName LIKE CONCAT('%',:ln ,'%')")
})
public class Etudiant implements InterfaceEntity  {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Basic
    @Column(name = "lastname", nullable = false)
    String lastName;

    @Basic
    @Column(name = "firstname", nullable = false)
    String firstName;

    public Etudiant(String firstnameStudent, String lastnameStudent) {
        this.firstName = firstnameStudent;
        this.lastName = lastnameStudent;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Etudiant that = (Etudiant) obj;
        return Objects.equals(id, that.id);
    }
}
