package fr.su.jakartaTp.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "formation", schema = "public")
@SqlResultSetMappings({
        @SqlResultSetMapping(name="Formation.EtudiantFormationMapping", entities = {
                @EntityResult(entityClass = Formation.class),
                @EntityResult(entityClass = Etudiant.class),
        })
})
@NamedQueries({
        @NamedQuery(name = "Formation.SelectAll", query = "SELECT f FROM Formation f"),
        @NamedQuery(name = "Formation.SelectByName", query = "SELECT f FROM Formation f WHERE f.name = :name"),
        @NamedQuery(name = "Formation.SelectById", query = "SELECT f FROM Formation f WHERE f.id = :id"),
        @NamedQuery(name = "Formation.SelectLikeByName", query = "SELECT f FROM Formation f WHERE f.name LIKE CONCAT('%', :name, '%')"),
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "Formation.SelectStudents", query =
                "SELECT DISTINCT f.name, e.id, e.lastname, e.firstname FROM formation f\n" +
                        "JOIN formation_to_cours ftc on f.id = ftc.formation_id\n" +
                        "JOIN cours_to_etudiant cte on ftc.cours_id = cte.cours_id\n" +
                        "JOIN etudiant e on e.id = cte.etudiant_id\n" +
                        "WHERE f.name LIKE CONCAT('%', :f_name, '%')",
                resultSetMapping = "Formation.EtudiantFormationMapping"
        )
})
public class Formation implements InterfaceEntity, Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    Integer id;

    @Basic
    @Column(name = "name", nullable = false)
    String name;

    @ManyToMany
    @JoinTable(
            name = "formation_to_enseignant",
            joinColumns = @JoinColumn(name = "formation_id"),
            inverseJoinColumns = @JoinColumn(name = "enseignant_id")
    )
    private List<Enseignant> enseignants = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "formation_to_cours",
            joinColumns = @JoinColumn(name = "formation_id"),
            inverseJoinColumns = @JoinColumn(name = "cours_id")
    )
    private List<Cours> cours = new ArrayList<>();

    public Formation(String name) {
        this.name = name;
    }
}
