package fr.su.jakartaTp.utils;

public class StringUtils {
    /**
     * Check if string is null or empty
     * @param s String
     * @return True if empty or null
     */
    public static boolean isEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static boolean isInteger(String s) {
        try {
            Integer.valueOf(s);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
