package fr.su.jakartaTp.utils;

public class Constants {
    public static class Results {
        public static String INFO = "info";
        public static String ERROR = "error";
        public static String SUCCESS = "success";

        public static String TYPE_FIELD = "status";
        public static String FIELD = "result";
    }

    public static class Messages {
        public static String IncompleteRequest = "Requête incomplète ou fausse";
        public static String RecordAlreadyPersisted = "Données déjà présentes dans l'application (doublon)";
        public static String CouldNotFindData = "Données correspondantes non trouvées";
        public static String NoActionSpecified = "Aucune action spécifiée";
    }

    public static class JspLocation {
        public static String Home = "./WEB-INF/index.jsp";

        public static String FormationAdd = "./WEB-INF/formation/addFormation.jsp";
        public static String FormationShow = "./WEB-INF/formation/showFormation.jsp";

        public static String StudentAdd = "./WEB-INF/etudiant/addEtudiant.jsp";
        public static String StudentShow = "./WEB-INF/etudiant/showEtudiant.jsp";

        public static String ClassAdd = "./WEB-INF/cours/addCours.jsp";
        public static String ClassShow = "./WEB-INF/cours/showCours.jsp";

        public static String TeacherAdd = "./WEB-INF/enseignant/addEnseignant.jsp";
        public static String TeacherShow = "./WEB-INF/enseignant/showEnseignant.jsp";
    }

    public static class URL {
        public static String Home = "";
        public static String Teacher = "enseignant";
        public static String Student = "etudiant";
        public static String Cours = "cours";
        public static String Formation = "formation";
    }
}
