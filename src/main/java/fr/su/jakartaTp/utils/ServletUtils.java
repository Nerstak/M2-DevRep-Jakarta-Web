package fr.su.jakartaTp.utils;

import fr.su.jakartaTp.models.SimpleResult;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ServletUtils  {
    public static void redirectToHome(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(Constants.Results.FIELD, Constants.Messages.NoActionSpecified);
        request.setAttribute(Constants.Results.TYPE_FIELD, Constants.Results.ERROR);
        redirect(response, Constants.URL.Home);
    }


    /**
     * Redirect to a controller
     * @param response Response
     * @param page Address of page
     */
    public static void redirect(HttpServletResponse response, String page) {
        try {
            response.sendRedirect(page);
        } catch (IOException e) {
            Logger.getLogger(ServletUtils.class.getName()).log(Level.SEVERE, "Redirection failed to " + page + ".", e);
        }
    }

    /**
     * Forward to a jsp
     * @param request Request
     * @param response Response
     * @param page Location of jsp
     */
    public static void forward(HttpServletRequest request, HttpServletResponse response, String page) {
        try {
            request.getRequestDispatcher(page).forward(request,response);
        }  catch (IOException | ServletException e) {
            Logger.getLogger(ServletUtils.class.getName()).log(Level.SEVERE, "Redirection failed to " + page + ".", e);
        }
    }

    /**
     * Save a response into the HttpRequest, in the correct fields
     * @param request HttpRequest
     * @param simpleResult Result
     */
    public static void saveSimpleResultInRequest(HttpServletRequest request, SimpleResult simpleResult) {
        request.setAttribute(Constants.Results.FIELD, simpleResult.message());
        request.setAttribute(Constants.Results.TYPE_FIELD, simpleResult.resultType());
    }
}
