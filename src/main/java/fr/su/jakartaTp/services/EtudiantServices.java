package fr.su.jakartaTp.services;

import fr.su.jakartaTp.dao.EtudiantDao;
import fr.su.jakartaTp.models.Etudiant;
import fr.su.jakartaTp.models.SimpleResult;
import fr.su.jakartaTp.utils.Constants;

import java.util.List;

import static fr.su.jakartaTp.utils.StringUtils.isEmpty;
import static fr.su.jakartaTp.utils.StringUtils.isInteger;

public class EtudiantServices {
    private final EtudiantDao etudiantDao = new EtudiantDao();

    /**
     * Add a student if possible
     * @param fname Firstname
     * @param lname Lastname
     * @return Result of query
     */
    public SimpleResult addEtudiant(String fname, String lname) {
        if (isEmpty(fname) || isEmpty(lname)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        if (etudiantDao.getEtudiantsByName(fname, lname).isEmpty()) {
            etudiantDao.save(new Etudiant(fname, lname));
            return new SimpleResult("Etudiant " + fname + " " + lname + " a été ajouté", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.RecordAlreadyPersisted, Constants.Results.INFO);
        }
    }

    /**
     * Get list of students (all or search)
     * @param fname Firstname (set to null if not needed)
     * @param lname Lastname (set to null if not needed)
     * @return List of students
     */
    public List<Etudiant> getEtudiants(String fname, String lname) {
        if (isEmpty(fname) && isEmpty(lname)) {
            return etudiantDao.getEtudiants();
        } else {
            return etudiantDao.getEtudiantsLikeByName(fname,lname);
        }
    }

    /**
     * Delete a student
     * @param etudiantId Student
     * @return Result of query
     */
    public SimpleResult remove(String etudiantId) {
        if (isEmpty(etudiantId) || !isInteger(etudiantId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var etudiant = etudiantDao.find(Integer.valueOf(etudiantId));

        if (etudiant != null) {
            etudiantDao.remove(etudiant);
            return new SimpleResult("Étudiant " + etudiant.getFirstName() + " " + etudiant.getLastName() + " a été supprimée", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
    }
}
