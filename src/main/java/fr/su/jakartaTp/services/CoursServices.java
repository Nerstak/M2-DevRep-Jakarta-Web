package fr.su.jakartaTp.services;

import fr.su.jakartaTp.dao.CoursDao;
import fr.su.jakartaTp.dao.EtudiantDao;
import fr.su.jakartaTp.models.Cours;
import fr.su.jakartaTp.models.Etudiant;
import fr.su.jakartaTp.models.SimpleResult;
import fr.su.jakartaTp.utils.Constants;

import java.util.List;

import static fr.su.jakartaTp.utils.StringUtils.isEmpty;
import static fr.su.jakartaTp.utils.StringUtils.isInteger;

public class CoursServices {
    private final CoursDao coursDao = new CoursDao();
    private final EtudiantDao etudiantDao = new EtudiantDao();

    /**
     * Add a cours
     * @param code Code
     * @param name Name
     * @return Result of query
     */
    public SimpleResult addCours(String code, String name) {
        if (isEmpty(code) || isEmpty(name)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        if (coursDao.getCoursByCode(code).isEmpty()) {
            coursDao.save(new Cours(code, name));
            return new SimpleResult("Cours " + code + ": " + name + " a été ajouté", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.RecordAlreadyPersisted, Constants.Results.INFO);
        }
    }

    /**
     * Get cours (all or search)
     * @param code Code (set to null if not used)
     * @param name Name (set to null if not used)
     * @return List of cours
     */
    public List<Cours> getCours(String code, String name) {
        if (isEmpty(code) && isEmpty(name)) {
            return coursDao.getCours();
        } else {
            return coursDao.getCoursLikeByCodeAndName(code, name);
        }
    }

    /**
     * Add etudiant to cours
     * @param fname Firstname of student
     * @param lname Lastname of student
     * @param code Code of cours
     * @return Result of query
     */
    public SimpleResult addEtudiantToCours(String fname, String lname, String code) {
        if(isEmpty(fname) || isEmpty(lname) || isEmpty(code)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        Cours cours = coursDao.getOneCoursByCode(code);
        Etudiant etudiant = etudiantDao.getEtudiantByName(fname, lname);

        if (cours == null) {
            return new SimpleResult("Cours: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (etudiant == null) {
            return new SimpleResult("Etudiant: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (!cours.getEtudiants().contains(etudiant)) {
            cours.getEtudiants().add(etudiant);
            coursDao.save(cours);
            return new SimpleResult("Étudiant " + fname + " " + lname + " a été ajouté au cours " + cours.getName(), Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.RecordAlreadyPersisted, Constants.Results.INFO);
        }
    }

    public SimpleResult remove(String coursId) {
        if (isEmpty(coursId) || !isInteger(coursId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var cours = coursDao.find(Integer.valueOf(coursId));

        if (cours != null) {
            coursDao.remove(cours);
            return new SimpleResult("Formation " + cours.getName() + " a été supprimée", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
    }

    public SimpleResult removeEtudiantFromCours(String coursId, String etudiantId) {
        if (isEmpty(etudiantId) || isEmpty(coursId) || !isInteger(etudiantId) || !isInteger(coursId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var etudiant = etudiantDao.find(Integer.valueOf(etudiantId));
        var cours = coursDao.find(Integer.valueOf(coursId));

        if (cours == null) {
            return new SimpleResult("Cours: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (etudiant == null) {
            return new SimpleResult("Etudiant: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (cours.getEtudiants().contains(etudiant)) {
            cours.getEtudiants().remove(etudiant);
            coursDao.save(cours);
            return new SimpleResult("Étudiant " + etudiant.getFirstName() + " " + etudiant.getLastName() + " a été enlevé du cours " + cours.getName(), Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Étudiant " + etudiant.getFirstName() + " " + etudiant.getLastName() +  " n'appartenait déjà pas au cours " + cours.getName(), Constants.Results.INFO);
        }
    }
}
