package fr.su.jakartaTp.services;

import fr.su.jakartaTp.dao.CoursDao;
import fr.su.jakartaTp.dao.EnseignantDao;
import fr.su.jakartaTp.dao.FormationDao;
import fr.su.jakartaTp.models.Cours;
import fr.su.jakartaTp.models.Enseignant;
import fr.su.jakartaTp.models.Formation;
import fr.su.jakartaTp.models.SimpleResult;
import fr.su.jakartaTp.utils.Constants;

import java.util.List;

import static fr.su.jakartaTp.utils.StringUtils.isEmpty;
import static fr.su.jakartaTp.utils.StringUtils.isInteger;

public class FormationServices {
    private final FormationDao formationDao = new FormationDao();
    private final CoursDao coursDao = new CoursDao();
    private final EnseignantDao enseignantDao = new EnseignantDao();

    /**
     * Add a formation if possible
     * @param name Name of formation
     * @return Result of query
     */
    public SimpleResult addFormation(String name) {
        if (isEmpty(name)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        if (formationDao.getFormationsByName(name).isEmpty()) {
            formationDao.save(new Formation(name));
            return new SimpleResult("Formation " + name + " a été ajouté", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.RecordAlreadyPersisted, Constants.Results.INFO);
        }
    }

    /**
     * Get list of formations (all or search)
     * @param name Name of formation (set to null if not needed)
     * @return List of formations
     */
    public List<Formation> getFormations(String name) {
        if (isEmpty(name)) {
            return formationDao.getFormations();
        } else {
            return formationDao.getFormationsLikeByName(name);
        }
    }

    /**
     * Get list of formations (all or search) and the students in them
     * @param name Name of formation (set to null if not needed)
     * @return List of formation associated with students
     */
    public List<FormationDao.FormationWithEtudiant> getFormationsWithStudents(String name) {
        return formationDao.getStudentsInFormation(name);
    }

    /**
     * Add a teacher to a formation
     * @param fname Firstname of teacher
     * @param lname Lastname of teacher
     * @param nameFormation Name of formation
     * @return Result of query
     */
    public SimpleResult addEnseignantToFormation(String fname, String lname, String nameFormation) {
        if (isEmpty(fname) || isEmpty(lname) || isEmpty(nameFormation)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        Formation formation = formationDao.getFormationByName(nameFormation);
        Enseignant enseignant = enseignantDao.getEnseignantByName(fname, lname);

        if (enseignant == null) {
            return new SimpleResult("Enseignant: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (formation == null) {
            return new SimpleResult("Formation: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (!formation.getEnseignants().contains(enseignant)) {
            formation.getEnseignants().add(enseignant);
            formationDao.save(formation);
            return new SimpleResult("Enseignant " + fname + " " + lname + " a été ajouté à la formation " + nameFormation, Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Enseignant " + fname + " " + lname + " a déjà été ajouté à la formation " + nameFormation, Constants.Results.INFO);
        }
    }

    /**
     * Add a cours to a formation
     * @param nameFormation Name of formation
     * @param code Code of cours
     * @return Result of query
     */
    public SimpleResult addCoursToFormation(String nameFormation, String code) {
        if (isEmpty(code) || isEmpty(nameFormation)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        Formation formation = formationDao.getFormationByName(nameFormation);
        Cours cours = coursDao.getOneCoursByCode(code);

        if (cours == null) {
            return new SimpleResult("Cours: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (formation == null) {
            return new SimpleResult("Formation: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (!formation.getCours().contains(cours)) {
            formation.getCours().add(cours);
            formationDao.save(formation);
            return new SimpleResult("Cours " + cours.getName() + " a été ajouté à la formation " + nameFormation, Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Cours " + cours.getName() + " a déjà été ajouté à la formation " + nameFormation, Constants.Results.INFO);
        }
    }

    /**
     * Delete a formation
     * @param formationId Formation
     * @return Result of query
     */
    public SimpleResult remove(String formationId) {
        if (isEmpty(formationId) || !isInteger(formationId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var formation = formationDao.find(Integer.valueOf(formationId));

        if (formation != null) {
            formationDao.remove(formation);
            return new SimpleResult("Formation " + formation.getName() + " a été supprimée", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
    }

    /**
     * Remove a cours from a formation
     * @param formationId Formation
     * @param coursId Cours
     * @return Result of query
     */
    public SimpleResult removeCoursFromFormation(String formationId, String coursId) {
        if (isEmpty(formationId) || isEmpty(coursId) || !isInteger(formationId) || !isInteger(coursId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var formation = formationDao.find(Integer.valueOf(formationId));
        var cours = coursDao.find(Integer.valueOf(coursId));

        if (cours == null) {
            return new SimpleResult("Cours: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (formation == null) {
            return new SimpleResult("Formation: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (formation.getCours().contains(cours)) {
            formation.getCours().remove(cours);
            formationDao.save(formation);
            return new SimpleResult("Cours " + cours.getName() + " a été enlevé de la formation " + formation.getName(), Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Cours " + cours.getName() + " n'appartenait déjà pas à la formation " + formation.getName(), Constants.Results.INFO);
        }
    }

    /**
     * Remove a teacher from a formation
     * @param formationId Formation
     * @param enseignantId Teacher
     * @return Result of query
     */
    public SimpleResult removeEnseignantFromFormation(String formationId, String enseignantId) {
        if (isEmpty(formationId) || isEmpty(enseignantId) || !isInteger(formationId) || !isInteger(enseignantId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var formation = formationDao.find(Integer.valueOf(formationId));
        var enseignant = enseignantDao.find(Integer.valueOf(enseignantId));

        if (enseignant == null) {
            return new SimpleResult("Enseignant: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (formation == null) {
            return new SimpleResult("Formation: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (formation.getEnseignants().contains(enseignant)) {
            formation.getEnseignants().remove(enseignant);
            formationDao.save(formation);
            return new SimpleResult("Enseignant " + enseignant.getFirstName() + " " + enseignant.getLastName() + " a été enlevé de la formation " + formation.getName(), Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Enseignant " + enseignant.getFirstName() + " " + enseignant.getLastName() + " n'appartenait déjà pas à la formation " + formation.getName(), Constants.Results.INFO);
        }
    }
}
