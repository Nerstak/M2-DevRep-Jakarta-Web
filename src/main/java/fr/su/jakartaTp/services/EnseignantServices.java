package fr.su.jakartaTp.services;

import fr.su.jakartaTp.dao.CoursDao;
import fr.su.jakartaTp.dao.EnseignantDao;
import fr.su.jakartaTp.models.Cours;
import fr.su.jakartaTp.models.Enseignant;
import fr.su.jakartaTp.models.SimpleResult;
import fr.su.jakartaTp.utils.Constants;

import java.util.List;

import static fr.su.jakartaTp.utils.StringUtils.isEmpty;
import static fr.su.jakartaTp.utils.StringUtils.isInteger;

public class EnseignantServices {
    private final EnseignantDao enseignantDao = new EnseignantDao();
    private final CoursDao coursDao = new CoursDao();

    /**
     * Add a teacher if possible
     * @param fname Firstname
     * @param lname Lastname
     * @return Result of query
     */
    public SimpleResult addEnseignant(String fname, String lname) {
        if (isEmpty(fname) || isEmpty(lname)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        if (enseignantDao.getEnseignantsByName(fname, lname).isEmpty()) {
            enseignantDao.save(new Enseignant(fname, lname));
            return new SimpleResult("Enseignant " + fname + " " + lname + " a été ajouté", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.RecordAlreadyPersisted, Constants.Results.INFO);
        }
    }


    /**
     * Get list of teachers (all or search)
     * @param fname Firstname (set to null if not needed)
     * @param lname Lastname (set to null if not needed)
     * @return List of teacher
     */
    public List<Enseignant> getEnseignants(String fname, String lname) {
        if (isEmpty(fname) && isEmpty(lname)) {
            return enseignantDao.getEnseignants();
        } else {
            return enseignantDao.getEnseignantsLikeByName(fname, lname);
        }
    }

    /**
     * Add cours to enseignant
     * @param fname Firstname of teacher
     * @param lname Lastname of teacher
     * @param code Code of cours
     * @return Result of query
     */
    public SimpleResult addCoursToEnseignant(String fname, String lname, String code) {
        if(isEmpty(fname) || isEmpty(lname) || isEmpty(code)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        Cours cours = coursDao.getOneCoursByCode(code);
        Enseignant enseignant = enseignantDao.getEnseignantByName(fname, lname);
        if (cours == null) {
            return new SimpleResult("Cours: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (enseignant == null) {
            return new SimpleResult("Enseignant: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (!enseignant.getCours().contains(cours)) {
            enseignant.getCours().add(cours);
            enseignantDao.save(enseignant);
            return new SimpleResult("Cours " + code + " a été attribué à l'enseignant " + fname + " " + lname, Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Cours " + code + " a déjà été attribué à l'enseignant " + fname + " " + lname, Constants.Results.SUCCESS);
        }
    }

    /**
     * Delete a teacher
     * @param enseignantId Teacher
     * @return Result of query
     */
    public SimpleResult remove(String enseignantId) {
        if (isEmpty(enseignantId) || !isInteger(enseignantId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var enseignant = enseignantDao.find(Integer.valueOf(enseignantId));

        if (enseignant != null) {
            enseignantDao.remove(enseignant);
            return new SimpleResult("Enseignant " + enseignant.getFirstName() + " " + enseignant.getLastName() + " a été supprimée", Constants.Results.SUCCESS);
        } else {
            return new SimpleResult(Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
    }

    /**
     * Remove a cours from a teacher
     * @param enseignantId Teacher
     * @param coursId Cours
     * @return Result of query
     */
    public SimpleResult removeCoursFromEnseignant(String enseignantId, String coursId) {
        if (isEmpty(enseignantId) || isEmpty(coursId) || !isInteger(enseignantId) || !isInteger(coursId)) {
            return new SimpleResult(Constants.Messages.IncompleteRequest, Constants.Results.ERROR);
        }

        var enseignant = enseignantDao.find(Integer.valueOf(enseignantId));
        var cours = coursDao.find(Integer.valueOf(coursId));

        if (cours == null) {
            return new SimpleResult("Cours: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }
        if (enseignant == null) {
            return new SimpleResult("Enseignant: " + Constants.Messages.CouldNotFindData, Constants.Results.ERROR);
        }

        if (enseignant.getCours().contains(cours)) {
            enseignant.getCours().remove(cours);
            enseignantDao.save(enseignant);
            return new SimpleResult("Cours " + cours.getName() + " a été enlevé du professeur " + enseignant.getFirstName() + " " + enseignant.getLastName(), Constants.Results.SUCCESS);
        } else {
            return new SimpleResult("Cours " + cours.getName() + " n'était pas attribué au professeur " + enseignant.getFirstName() + " " + enseignant.getLastName(), Constants.Results.INFO);
        }
    }
}
