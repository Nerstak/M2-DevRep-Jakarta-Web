package fr.su.jakartaTp.dao;

import fr.su.jakartaTp.models.Cours;
import jakarta.persistence.Query;

import java.util.ArrayList;
import java.util.List;

public class CoursDao extends ModelDao<Cours> {
    @Override
    public Cours find(Integer id) {
        var em = emf.createEntityManager();
        var res = em.find(Cours.class, id);
        em.close();
        return res;
    }

    public List<Cours> getCoursByCode(String s) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Cours.SelectByCode");
        q.setParameter("code", s == null ? "" : s);

        @SuppressWarnings("unchecked")
        List<Cours> res = q.getResultList();
        em.close();

        return res;
    }

    public Cours getOneCoursByCode(String s) {
        ArrayList<Cours> list = new ArrayList<>();
        list.addAll(getCoursByCode(s));

        Cours res;
        if(!list.isEmpty()) {
            res = list.get(0);
        } else {
            res = null;
        }

        return res;
    }

    public List<Cours> getCoursLikeByCodeAndName(String code, String name) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Cours.SelectLikeByCodeAndName");
        q.setParameter("code", code == null ? "" : code);
        q.setParameter("name", name == null ? "" : name);
        @SuppressWarnings("unchecked")
        List<Cours> res = q.getResultList();
        em.close();

        return res;
    }

    public List<Cours> getCours() {
        var em = emf.createEntityManager();
        Query q = em.createNamedQuery("Cours.SelectAll");
        @SuppressWarnings("unchecked")
        List<Cours> res = q.getResultList();
        em.close();
        return res;
    }
}
