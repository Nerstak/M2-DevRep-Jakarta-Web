package fr.su.jakartaTp.dao;

import fr.su.jakartaTp.models.Etudiant;
import jakarta.persistence.Query;

import java.util.ArrayList;
import java.util.List;

public class EtudiantDao extends ModelDao<Etudiant> {
    @Override
    public Etudiant find(Integer id) {
        var em = emf.createEntityManager();
        var res = em.find(Etudiant.class, id);
        em.close();
        return res;
    }

    public List<Etudiant> getEtudiants() {
        var em = emf.createEntityManager();
        Query q = em.createNamedQuery("Etudiant.SelectAll");
        @SuppressWarnings("unchecked")
        List<Etudiant> res = q.getResultList();
        em.close();
        return res;
    }

    public List<Etudiant> getEtudiantsByName(String first, String last) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Etudiant.SelectByNames");
        q.setParameter("ln", last == null ? "" : last);
        q.setParameter("fn", first == null ? "" : first);
        @SuppressWarnings("unchecked")
        List<Etudiant> res = q.getResultList();
        em.close();

        return res;
    }

    public List<Etudiant> getEtudiantsLikeByName(String first, String last) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Etudiant.SelectLikeByNames");
        q.setParameter("ln", last == null ? "" : last);
        q.setParameter("fn", first == null ? "" : first);

        @SuppressWarnings("unchecked")
        List<Etudiant> res = q.getResultList();
        em.close();

        return res;
    }

    public Etudiant getEtudiantByName(String first, String last) {
        ArrayList<Etudiant> list = new ArrayList<>();
        list.addAll(getEtudiantsByName(first, last));

        Etudiant res;
        if(!list.isEmpty()) {
            res = list.get(0);
        } else {
            res = null;
        }

        return res;
    }
}

