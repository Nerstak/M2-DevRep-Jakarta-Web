package fr.su.jakartaTp.dao;

import fr.su.jakartaTp.models.InterfaceEntity;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;

/**
 * Abstract class for database manipulation
 * @param <T> Entity
 */
public abstract class ModelDao<T extends InterfaceEntity> {
    protected EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

    public void save(T e) {
        var em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(e);
        em.getTransaction().commit();
        em.close();
    }

    public void remove(T e) {
        var em = emf.createEntityManager();
        em.getTransaction().begin();

        if (!em.contains(e)) {
            e = em.merge(e);
        }

        em.remove(e);
        em.getTransaction().commit();
        em.close();
    }

    public abstract T find(Integer id);
}
