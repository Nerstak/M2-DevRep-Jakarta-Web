package fr.su.jakartaTp.dao;

import fr.su.jakartaTp.models.Etudiant;
import fr.su.jakartaTp.models.Formation;
import jakarta.persistence.Query;

import java.util.ArrayList;
import java.util.List;


public class FormationDao extends ModelDao<Formation> {
    public record FormationWithEtudiant(Formation formation, Etudiant etudiant) {}

    @Override
    public Formation find(Integer id) {
        var em = emf.createEntityManager();
        var res = em.find(Formation.class, id);
        em.close();
        return res;
    }

    public List<Formation> getFormations() {
        var em = emf.createEntityManager();
        Query q = em.createNamedQuery("Formation.SelectAll");

        @SuppressWarnings("unchecked")
        List<Formation> res = q.getResultList();
        em.close();
        return res;
    }

    public List<Formation> getFormationsByName(String s) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Formation.SelectByName");
        q.setParameter("name", s == null ? "":s);
        @SuppressWarnings("unchecked")
        List<Formation> res = q.getResultList();
        em.close();

        return res;
    }
    public Formation getFormationByName(String s) {
        ArrayList<Formation> list = new ArrayList<>();
        list.addAll(getFormationsByName(s));

        Formation res;
        if(!list.isEmpty()) {
            res = list.get(0);
        } else {
            res = null;
        }

        return res;
    }

    /**
     * Get students in a formation
     * @param s Name of formation
     * @return List of formation with student
     */
    public List<FormationWithEtudiant> getStudentsInFormation(String s) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Formation.SelectStudents");
        q.setParameter("f_name", s == null ? "": s);

        @SuppressWarnings("unchecked")
        List res = q.getResultList().stream().map(x -> {
            var objects = (Object[]) x;
            return new FormationWithEtudiant((Formation) objects[0], (Etudiant) objects[1]);
        }).toList();
        em.close();

        return res;
    }

    public List<Formation> getFormationsLikeByName(String name) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Formation.SelectLikeByName");
        q.setParameter("name", name == null ? "" : name);

        @SuppressWarnings("unchecked")
        List<Formation> res = q.getResultList();
        em.close();

        return res;
    }
}
