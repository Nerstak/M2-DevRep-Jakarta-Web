package fr.su.jakartaTp.dao;

import fr.su.jakartaTp.models.Enseignant;
import jakarta.persistence.Query;

import java.util.ArrayList;
import java.util.List;

public class EnseignantDao extends ModelDao<Enseignant> {
    @Override
    public Enseignant find(Integer id) {
        var em = emf.createEntityManager();
        var res = em.find(Enseignant.class, id);
        em.close();
        return res;
    }

    public List<Enseignant> getEnseignants() {
        var em = emf.createEntityManager();
        Query q = em.createNamedQuery("Enseignant.SelectAll");

        @SuppressWarnings("unchecked")
        List<Enseignant> res = q.getResultList();
        em.close();
        return res;
    }

    public List<Enseignant> getEnseignantsByName(String first, String last) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Enseignant.SelectByNames");
        q.setParameter("ln", last == null ? "" : last);
        q.setParameter("fn", first == null ? "" : first);

        @SuppressWarnings("unchecked")
        List<Enseignant> res = q.getResultList();
        em.close();

        return res;
    }

    public List<Enseignant> getEnseignantsLikeByName(String first, String last) {
        var em = emf.createEntityManager();

        Query q = em.createNamedQuery("Enseignant.SelectLikeByNames");
        q.setParameter("ln", last == null ? "" : last);
        q.setParameter("fn", first == null ? "" : first);

        @SuppressWarnings("unchecked")
        List<Enseignant> res = q.getResultList();
        em.close();

        return res;
    }

    public Enseignant getEnseignantByName(String first, String last) {
        ArrayList<Enseignant> list = new ArrayList<>();
        list.addAll(getEnseignantsByName(first, last));

        Enseignant res;
        if(!list.isEmpty()) {
            res = list.get(0);
        } else {
            res = null;
        }

        return res;
    }
}
