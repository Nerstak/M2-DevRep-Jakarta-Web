package fr.su.jakartaTp.controllers;

import fr.su.jakartaTp.services.CoursServices;
import fr.su.jakartaTp.utils.Constants;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;

import static fr.su.jakartaTp.utils.ServletUtils.*;

@WebServlet(name = "CoursServlet", value = "/cours")
public class CoursServlet extends HttpServlet {
    private final CoursServices coursServices = new CoursServices();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            redirect(response, Constants.URL.Cours + "?action=show");
            return;
        }

        switch (Objects.requireNonNull(action)) {
            case "add" -> {
                forward(request, response, Constants.JspLocation.ClassAdd);
            }
            default -> { // Contains "show" action
                showClasses(request, response);
            }
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("actionForm");
        if (action == null) {
            redirectToHome(request, response);
            return;
        }

        switch (Objects.requireNonNull(action)) {
            case "add" -> {
                addCours(request);
                forward(request, response,Constants.JspLocation.ClassAdd);
            }
            case "add-etudiant" -> {
                addEtudiantToCours(request);
                forward(request, response,Constants.JspLocation.ClassAdd);
            }
            case "delete" -> {
                deleteCours(request);
                redirect(response, Constants.URL.Cours);
            }
            case "delete-etudiant" -> {
                deleteEtudiantFromCours(request);
                redirect(response, Constants.URL.Cours);
            }
            default -> {
                redirectToHome(request, response);
            }
        }
    }

    /**
     * Remove a student from cours
     * @param request HttpReq
     */
    private void deleteEtudiantFromCours(HttpServletRequest request) {
        String etudiantId = request.getParameter("etudiantId");
        String coursId = request.getParameter("coursId");

        var res = coursServices.removeEtudiantFromCours(coursId, etudiantId);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Remove a cours
     * @param request HttpReq
     */
    private void deleteCours(HttpServletRequest request) {
        String id = request.getParameter("coursId");

        var res = coursServices.remove(id);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Add student to classe
     * @param request HttpReq
     */
    private void addEtudiantToCours(HttpServletRequest request) {
        String firstnameEtudiant = request.getParameter("firstnameEtudiant");
        String lastnameEtudiant = request.getParameter("lastnameEtudiant");
        String codeCours = request.getParameter("codeCours");

        var res = coursServices.addEtudiantToCours(firstnameEtudiant, lastnameEtudiant, codeCours);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Add cours if it does not already exist
     * @param request HttpRequest
     */
    private void addCours(HttpServletRequest request) {
        String codeCours = request.getParameter("codeCours");
        String nomCours = request.getParameter("nameCours");

        var res = coursServices.addCours(codeCours, nomCours);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Show and search classes
     * @param request User request
     */
    private void showClasses(HttpServletRequest request, HttpServletResponse response) {
        String code = request.getParameter("codeCours");
        String name = request.getParameter("nomCours");

        request.setAttribute("coursToStudents", coursServices.getCours(code, name));
        forward(request, response,Constants.JspLocation.ClassShow);
    }
}
