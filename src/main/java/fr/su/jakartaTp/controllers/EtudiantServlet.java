package fr.su.jakartaTp.controllers;

import fr.su.jakartaTp.services.EtudiantServices;
import fr.su.jakartaTp.utils.Constants;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;

import static fr.su.jakartaTp.utils.ServletUtils.*;


@WebServlet(name = "EtudiantServlet", value = "/etudiant")
public class EtudiantServlet extends HttpServlet {
    private final EtudiantServices etudiantServices = new EtudiantServices();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            redirect(response, Constants.URL.Student + "?action=show");
            return;
        }
        switch (Objects.requireNonNull(action)) {
            case "add" -> forward(request, response, Constants.JspLocation.StudentAdd);
            default -> { // Includes "show"
                showStudents(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("actionForm");
        if (action == null) {
            redirectToHome(request, response);
            return;
        }

        switch (action) {
            case "add" -> {
                addEtudiant(request);
                forward(request, response, Constants.JspLocation.StudentAdd);
            }
            case "delete" -> {
                deleteEtudiant(request);
                redirect(response, Constants.URL.Student);
            }
            default -> {
                redirectToHome(request, response);
            }
        }
    }

    /**
     * Remove a student
     *
     * @param request HttpReq
     */
    private void deleteEtudiant(HttpServletRequest request) {
        String id = request.getParameter("etudiantId");

        var res = etudiantServices.remove(id);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Add student
     *
     * @param request User request
     */
    private void addEtudiant(HttpServletRequest request) {
        String firstnameStudent = request.getParameter("firstnameStudent");
        String lastnameStudent = request.getParameter("lastnameStudent");

        var res = etudiantServices.addEtudiant(firstnameStudent, lastnameStudent);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Show students
     *
     * @param request User request
     */
    private void showStudents(HttpServletRequest request, HttpServletResponse response) {
        String firstnameStudent = request.getParameter("firstnameStudent");
        String lastnameStudent = request.getParameter("lastnameStudent");

        request.setAttribute("listEtudiant", etudiantServices.getEtudiants(firstnameStudent, lastnameStudent));
        forward(request, response, Constants.JspLocation.StudentShow);
    }
}
