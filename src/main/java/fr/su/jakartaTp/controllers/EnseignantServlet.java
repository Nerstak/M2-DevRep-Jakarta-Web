package fr.su.jakartaTp.controllers;

import fr.su.jakartaTp.services.EnseignantServices;
import fr.su.jakartaTp.utils.Constants;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;
import static fr.su.jakartaTp.utils.ServletUtils.*;


@WebServlet(name = "EnseignantServlet", value = "/enseignant")
public class EnseignantServlet extends HttpServlet {
    private final EnseignantServices enseignantServices = new EnseignantServices();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action == null) {
            redirect(response, Constants.URL.Teacher + "?action=show");
            return;
        }

        switch (Objects.requireNonNull(action)) {
            case "add" -> {
                forward(request, response, Constants.JspLocation.TeacherAdd);
            }
            default -> { // Contains case "show"
                showTeachers(request, response);
            }
        }
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("actionForm");
        if (action == null) {
            redirectToHome(request, response);
            return;
        }

        switch (Objects.requireNonNull(action)) {
            case "add" -> {
                addEnseignant(request);
                forward(request, response, Constants.JspLocation.TeacherAdd);
            }
            case "add-cours" -> {
                addCoursToEnseignant(request);
                forward(request, response, Constants.JspLocation.TeacherAdd);
            }
            case "delete" -> {
                deleteTeacher(request);
                redirect(response, Constants.URL.Teacher);
            }
            case "delete-cours" -> {
                deleteCoursFromTeacher(request);
                redirect(response, Constants.URL.Teacher);
            }
            default -> {
                redirectToHome(request, response);
            }
        }
    }

    /**
     * Remove a cours from a teacher
     * @param request HttpReq
     */
    private void deleteCoursFromTeacher(HttpServletRequest request) {
        String enseignantId = request.getParameter("enseignantId");
        String coursId = request.getParameter("coursId");

        var res = enseignantServices.removeCoursFromEnseignant(enseignantId, coursId);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Remove a teacher
     * @param request HttpReq
     */
    private void deleteTeacher(HttpServletRequest request) {
        String id = request.getParameter("enseignantId");

        var res = enseignantServices.remove(id);
        saveSimpleResultInRequest(request, res);
    }


    /**
     * Add class to teacher
     *
     * @param request User request
     */
    private void addCoursToEnseignant(HttpServletRequest request) {
        String firstnameTeacher = request.getParameter("firstnameTeacher");
        String lastnameTeacher = request.getParameter("lastnameTeacher");
        String codeCours = request.getParameter("codeCours");

        var res = enseignantServices.addCoursToEnseignant(firstnameTeacher, lastnameTeacher, codeCours);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Add teacher
     *
     * @param request User request
     */
    private void addEnseignant(HttpServletRequest request) {
        String firstnameTeacher = request.getParameter("firstnameTeacher");
        String lastnameTeacher = request.getParameter("lastnameTeacher");

        var res = enseignantServices.addEnseignant(firstnameTeacher, lastnameTeacher);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Show and search teachers
     *
     * @param request  User request
     * @param response Response
     */
    private void showTeachers(HttpServletRequest request, HttpServletResponse response) {
        String firstnameTeacher = request.getParameter("firstnameTeacher");
        String lastnameTeacher = request.getParameter("lastnameTeacher");

        request.setAttribute("enseignantToCours", enseignantServices.getEnseignants(firstnameTeacher, lastnameTeacher));
        forward(request, response, Constants.JspLocation.TeacherShow);
    }
}
