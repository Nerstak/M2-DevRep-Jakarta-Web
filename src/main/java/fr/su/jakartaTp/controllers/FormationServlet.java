package fr.su.jakartaTp.controllers;

import fr.su.jakartaTp.services.FormationServices;
import fr.su.jakartaTp.utils.Constants;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;

import java.io.IOException;
import java.util.Objects;
import static fr.su.jakartaTp.utils.ServletUtils.*;


@WebServlet(name = "FormationServlet", value = "/formation")
public class FormationServlet extends HttpServlet {
    private final FormationServices formationServices = new FormationServices();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");

        if (action == null) {
            redirect(response, Constants.URL.Formation + "?action=show");
            return;
        }

        switch (Objects.requireNonNull(action)) {
            case "add" -> {
                forward(request, response, Constants.JspLocation.FormationAdd);
            }
            default -> { // Include case "show"
                showFormations(request, response);
            }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("actionForm");

        if (action == null) {
            redirectToHome(request, response);
            return;
        }

        switch (Objects.requireNonNull(action)) {
            case "add" -> {
                addFormation(request);
                forward(request, response, Constants.JspLocation.FormationAdd);
            }
            case "add-cours" -> {
                addCoursToFormation(request);
                forward(request, response, Constants.JspLocation.FormationAdd);
            }
            case "add-enseignant" -> {
                addEnseignantToFormation(request);
                forward(request, response, Constants.JspLocation.FormationAdd);
            }
            case "delete" -> {
                deleteFormation(request);
                redirect(response, Constants.URL.Formation);
            }
            case "delete-cours" -> {
                deleteCoursFromFormation(request);
                redirect(response, Constants.URL.Formation);
            }
            case "delete-enseignant" -> {
                deleteEnseignantFromFormation(request);
                redirect(response, Constants.URL.Formation);
            }
            default -> {
                redirectToHome(request, response);
            }
        }
}

    /**
     * Remove a teacher from a formation
     * @param request HttpReq
     */
    private void deleteEnseignantFromFormation(HttpServletRequest request) {
        String formationId = request.getParameter("formationId");
        String enseignantId = request.getParameter("enseignantId");

        var res = formationServices.removeEnseignantFromFormation(formationId, enseignantId);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Remove a cours from a formation
     * @param request HttpReq
     */
    private void deleteCoursFromFormation(HttpServletRequest request) {
        String formationId = request.getParameter("formationId");
        String coursId = request.getParameter("coursId");

        var res = formationServices.removeCoursFromFormation(formationId, coursId);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Remove a formation
     * @param request HttpReq
     */
    private void deleteFormation(HttpServletRequest request) {
        String id = request.getParameter("formationId");

        var res = formationServices.remove(id);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Select and prepare the formations (and linked data) based on research
     *
     * @param request User request
     * @param response Response
     */
    private void showFormations(HttpServletRequest request, HttpServletResponse response) {
        String t = request.getParameter("typeSelected");
        String name = request.getParameter("nameFormation");

        // Getting the students is the only "complex" request
        if (t != null && t.equals("etudiant")) {
            request.setAttribute("formations", formationServices.getFormationsWithStudents(name));
        } else {
            request.setAttribute("formations", formationServices.getFormations(name));
        }
        forward(request, response, Constants.JspLocation.FormationShow);
    }


    /**
     * Add teacher to formation
     *
     * @param request User request
     */
    private void addEnseignantToFormation(HttpServletRequest request) {
        String name = request.getParameter("nomFormation");
        String firstNameEnseignant = request.getParameter("firstnameTeacher");
        String lastNameEnseignant = request.getParameter("lastnameTeacher");

        var res = formationServices.addEnseignantToFormation(firstNameEnseignant, lastNameEnseignant, name);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Add a class to a formation
     *
     * @param request User request
     */
    private void addCoursToFormation(HttpServletRequest request) {
        String name = request.getParameter("nomFormation");
        String codeCours = request.getParameter("codeCours");

        var res = formationServices.addCoursToFormation(name, codeCours);
        saveSimpleResultInRequest(request, res);
    }

    /**
     * Add a formation based on given user inputs
     *
     * @param request User request
     */
    private void addFormation(HttpServletRequest request) {
        String name = request.getParameter("nomFormation");

        var res = formationServices.addFormation(name);
        saveSimpleResultInRequest(request, res);
    }
}
